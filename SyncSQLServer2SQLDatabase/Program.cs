﻿using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;
using Microsoft.Synchronization;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SyncSQLServer2SQLDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            string LocalSQLServerConnectionString = "Data Source=localhost; Initial Catalog=SyncDB;Integrated Security=True";
            string RemoteSQLAzureConnectionString = "Server=tcp:dzywf1vjbu.database.chinacloudapi.cn,1433;Database=test;User ID=example@dzywf1vjbu;Password=#C29ddec;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string scopeName = "PScope";
            using (SqlConnection sqlServerConn = new SqlConnection(LocalSQLServerConnectionString))
            {
                using (SqlConnection sqlAzureConn =
                    new SqlConnection(RemoteSQLAzureConnectionString))
                {
                    //定义同步域
                    DbSyncScopeDescription myScope =
                        new DbSyncScopeDescription(scopeName);

                    //获取需要同步的表
                    DbSyncTableDescription Product =
                        SqlSyncDescriptionBuilder.GetDescriptionForTable("Products", sqlServerConn);

                    // Add the tables from above to the scope
                    myScope.Tables.Add(Product);

                    //主数据库处理同步结构
                    SqlSyncScopeProvisioning sqlServerProv = new SqlSyncScopeProvisioning(sqlServerConn, myScope);
                    if (!sqlServerProv.ScopeExists(scopeName))
                    {
                        // Apply the scope provisioning.
                        sqlServerProv.Apply();
                    }

                    SqlSyncScopeDeprovisioning scopeDep = new SqlSyncScopeDeprovisioning(sqlServerConn);
                    scopeDep.DeprovisionScope(scopeName);
                    


                    //将主数据同步接口复制到从数据库
                    SqlSyncScopeProvisioning sqlAzureProv = new SqlSyncScopeProvisioning(sqlAzureConn, myScope);
                    if (!sqlAzureProv.ScopeExists(scopeName))
                        // Apply the scope provisioning.
                        sqlAzureProv.Apply();
                }
            }


            //开始执行同步
            using (SqlConnection sqlServerConn = new SqlConnection(LocalSQLServerConnectionString))
            {
                using (SqlConnection sqlAzureConn = new SqlConnection(RemoteSQLAzureConnectionString))
                {
                    //建立一个代理
                    SyncOrchestrator syncOrchestrator = new SyncOrchestrator
                    {
                        //指定本地数据库
                        LocalProvider = new SqlSyncProvider(scopeName, sqlServerConn),
                        //指定远程数据库
                        RemoteProvider = new SqlSyncProvider(scopeName, sqlAzureConn),
                        //设置同步方向
                        Direction = SyncDirectionOrder.UploadAndDownload
                    };
                    //开始同步
                    var syncStats = syncOrchestrator.Synchronize();
                    Console.WriteLine("Start Time: " + syncStats.SyncStartTime);
                    Console.WriteLine("Total Changes Uploaded: " + syncStats.UploadChangesTotal);
                    Console.WriteLine("Total Changes Downloaded: " + syncStats.DownloadChangesTotal);
                    Console.WriteLine("Complete Time: " + syncStats.SyncEndTime);
                    Console.WriteLine(String.Empty);
                }
            }


        }
    }
}
